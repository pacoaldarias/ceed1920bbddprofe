SET SERVEROUTPUT ON
DECLARE
I INT := 1;
BEGIN
WHILE I <= 5 LOOP
    UPDATE GAMASPRODUCTOS
    SET DESCRIPCIONTEXTO =  'LA GAMA ' || I || ' ESTA DISPONIBLE'
    WHERE GAMA = 'GAMA_' || I;
    I := I + 1;
END LOOP;
END;
/