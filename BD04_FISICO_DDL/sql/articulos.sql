﻿
DROP TABLE linea;
DROP TABLE pedido;
DROP TABLE articulo;
DROP TABLE cliente;



 CREATE TABLE articulo (
  id int,
  nombre varchar(45),
  precio decimal (4,2),
  constraint pk_articulo_id PRIMARY KEY (id)
);

INSERT INTO articulo (id, nombre, precio) VALUES (1, 'guantes', 10.15 );
INSERT INTO articulo (id, nombre, precio) VALUES (2, 'jerseys', 5.05  );
INSERT INTO articulo (id, nombre, precio) VALUES (3, 'zapatos', 2.65  );

CREATE TABLE cliente (
   id INT,
   nombre  VARCHAR(45) ,
   email   VARCHAR(45) ,
   usuario VARCHAR(45),
   password VARCHAR(45),
   fechaNacimiento DATE ,
  constraint pk_cliente_id  PRIMARY KEY (id) 
);

INSERT INTO cliente (id, nombre, email, usuario, password, fechanacimiento ) VALUES (1, 'Juan', 'juan@gmail.com', 'juan','1234', '23/10/2019' );
INSERT INTO cliente (id, nombre, email, usuario, password, fechanacimiento ) VALUES (2, 'Paco', 'paco@gmail.com', 'paco','1234', '17/11/2019' );
INSERT INTO cliente (id, nombre, email, usuario, password, fechanacimiento ) VALUES (3, 'Pedro', 'pedro@gmail.com', 'pedro','1234', '18/10/2019' );


CREATE TABLE pedido (
  id INT ,
  fecha DATE ,
  clienteid INT NOT NULL,
  CONSTRAINT pk_pedido_id  PRIMARY KEY (id),
  CONSTRAINT fk_pedido_cliente
    FOREIGN KEY (clienteid)
    REFERENCES cliente (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

INSERT INTO pedido (id, fecha, clienteid ) VALUES (1, '18/10/2019', 1 );
INSERT INTO pedido (id, fecha, clienteid ) VALUES (2, '19/10/2019', 2 );
INSERT INTO pedido (id, fecha, clienteid ) VALUES (3, '20/10/2019', 3 );

CREATE TABLE linea (
  numero INT ,
  pedidoid INT, 
  articuloid INT ,
  precio DECIMAL (4,2),
  cantidad INT,
  CONSTRAINT pk_linea  PRIMARY KEY (numero, pedidoid), 
  CONSTRAINT fk_pedido_id  FOREIGN KEY (pedidoid)
    REFERENCES pedido (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_articulo_id  FOREIGN KEY (articuloid)
    REFERENCES articulo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

INSERT INTO linea (pedidoid, numero,  articuloid, cantidad, precio )  VALUES (1, 1, 1, 3, 0 );
INSERT INTO linea (pedidoid, numero,  articuloid, cantidad, precio )  VALUES (1, 2, 2, 2, 0 );
INSERT INTO linea (pedidoid, numero,  articuloid, cantidad, precio )  VALUES (1, 3, 3, 5, 0 );

UPDATE linea set precio = (select precio from articulo where  linea.articuloid=articulo.id);
