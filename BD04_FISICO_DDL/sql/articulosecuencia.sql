﻿CREATE SEQUENCE mydb.seq_articuloid
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

ALTER TABLE mydb.seq_articuloid
  OWNER TO postgres;
COMMENT ON SEQUENCE mydb.seq_articuloid
  IS 'Secuencia que se utiliza para la generación de la clave  primaria ';



CREATE TABLE articulo (
  id INT NULL,
  nombre VARCHAR(45) NULL,
  precio DOUBLE NULL,
  constraint pk_articulo_id PRIMARY KEY (id)
);

ALTER TABLE articulo.id
alter column id set default nexval('articulo.seq_id');

insert into articulo (nombre, precio,stock) values (‘guantes’,10.0, 3);