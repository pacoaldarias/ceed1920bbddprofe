

-- -----------------------------------------------------
-- Table Articulo
-- -----------------------------------------------------
CREATE TABLE Articulo (
  idArticulo INT NULL,
  Nombre VARCHAR(45) NULL,
  Precio DOUBLE NULL,
  constraint pk_articulo_id PRIMARY KEY (idArticulo)
);


-- -----------------------------------------------------
-- Table Cliente
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cliente` (
  `idCliente` INT NULL,
  `Nombre` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `Usuario` VARCHAR(45) NULL,
  `Password` VARCHAR(45) NULL,
  `FechaNacimiento` DATE NULL,
  PRIMARY KEY (`idCliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Pedido` (
  `idPedido` INT NULL,
  `Fecha` DATE NULL,
  `Cliente_idCliente` INT NOT NULL,
  PRIMARY KEY (`idPedido`, `Cliente_idCliente`),
  INDEX `fk_Pedido_Cliente1_idx` (`Cliente_idCliente` ASC),
  CONSTRAINT `fk_Pedido_Cliente1`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `mydb`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Linea`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Linea` (
  `idLinea` INT NULL,
  `Cantidad` INT(4) NULL,
  `Fecha` DATE NULL,
  `Pedido_idPedido` INT NOT NULL,
  `Pedido_Cliente_idCliente` INT NOT NULL,
  `Articulo_idArticulo` INT NOT NULL,
  PRIMARY KEY (`idLinea`, `Articulo_idArticulo`),
  INDEX `fk_Linea_Pedido1_idx` (`Pedido_idPedido` ASC, `Pedido_Cliente_idCliente` ASC),
  INDEX `fk_Linea_Articulo1_idx` (`Articulo_idArticulo` ASC),
  CONSTRAINT `fk_Linea_Pedido1`
    FOREIGN KEY (`Pedido_idPedido` , `Pedido_Cliente_idCliente`)
    REFERENCES `mydb`.`Pedido` (`idPedido` , `Cliente_idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Linea_Articulo1`
    FOREIGN KEY (`Articulo_idArticulo`)
    REFERENCES `mydb`.`Articulo` (`idArticulo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
