-- drop table departamentos,  empleados;

create table departamentos (
    CodDpto varchar(10) primary key,
    Nombre varchar(30) not null,
    uticacion varchar(30)
    ) ENGINE=InnoDB;

insert into departamentos values('INF','Informatica','Planta Sotano U3');
insert into departamentos values('ADM','Administración','Planta Quinta U2');
insert into departamentos values('COM','Comercial','Planta Tercera U3');
insert into departamentos values('CONT','Contabilidad','Planta Quinta U1');
insert into departamentos values('ALM','Almacen','Planta Baja U1');

create table empleados (
dni varchar (10) ,
nombre varchar (30) ,
especialidad varchar(25),
fechaalta date,
dpto varchar(10),
primary key ( dni ) ,
foreign key ( dpto ) references departamentos ( CodDpto )
on delete CASCADE ON UPDATE CASCADE
) ENGINE = Innodb;

insert into empleados values('12345678A','Alberto Gil','Contable', '2010/12/10','CONT');
insert into empleados values('23456789B','Mariano Sanz','Informática', '2010/10/4','INF');
insert into empleados values('34567890C','Iván Gómez','Ventas', '2020/07/20','COM');
insert into empleados values('45678901D','Ana Silván','Informática', '2012/11/25','INF');
insert into empleados values('56789012E','Maria Cuadrado','Ventas', '2013/2/4','COM');
insert into empleados values('67890123F','Roberto Milán','Logística', '2010/2/5','ALM');

-- tema 6
alter table empleados add Sueldo float;
update empleados set Sueldo = 1500 wher dni in ('45678901D','67890123F');
