-- base de datos tienda
-- datos evaluable 4. SQL

DROP TABLE IF EXISTS articulo_recomendado;
DROP TABLE IF EXISTS lineas_compra;
DROP TABLE IF EXISTS lineas_venta;
DROP TABLE IF EXISTS telefonos;
DROP TABLE IF EXISTS tipos_articulos;
DROP TABLE IF EXISTS tipos;
DROP TABLE IF EXISTS articulos;
DROP TABLE IF EXISTS ventas;
DROP TABLE IF EXISTS compras;
DROP TABLE IF EXISTS clientes;
DROP TABLE IF EXISTS proveedores;
DROP TABLE IF EXISTS usuarios;
--
CREATE TABLE usuarios(
Id_usu CHAR(9),
Nombre VARCHAR(50),
Primer_apellido VARCHAR(50),
Segundo_apellido VARCHAR(50),
Ciudad VARCHAR(50),
CP CHAR(6),
Calle VARCHAR(50),
Numero VARCHAR(10),
Piso VARCHAR(10),
Pta VARCHAR(10),
Usuario VARCHAR(15) NOT NULL UNIQUE,
Password VARCHAR(15) NOT NULL,
Email VARCHAR(50),
CONSTRAINT USU_IDU_PK PRIMARY KEY(Id_usu)
);
--
INSERT INTO usuarios (Id_usu, Nombre, Primer_apellido, Segundo_apellido, 
Ciudad, CP, Calle, Numero, Piso, Pta, Usuario, Password, Email)
VALUES ('001', 'José', 'Alfaro', 'García', 'Albacete', '02006',
'María Marín', '57', '03', 'B', 'Pepin33', 'pepin', 'pepin@hotmail.es'),
('002', 'Miguel', 'Perez', 'Pena', 'Albacete', '02007',
'Perez Pastor', '41', '02', 'A', 'Mike47', '1234', 'mikel@hotmail.es'),
('003', 'Ana', 'Ponce', 'García', 'Madrid', '28001',
'Gran Via', '07', '10', 'F', 'Anika12', '0123456', 'anika@hotmail.es'),
('004', 'Antonio', 'Fernandez', 'Ruiz', 'Valencia', '46008',
'San Vicente', '11', '5', 'F', 'antru', '7777777', 'antru@gmail.es')
;
--
CREATE TABLE telefonos(
Id_telf CHAR(9),
Telefono VARCHAR(20),
Id_usu CHAR(9),
CONSTRAINT TEL_IDT_PK PRIMARY KEY(Id_telf),
CONSTRAINT TEL_IDU_FK FOREIGN KEY(Id_usu) REFERENCES usuarios (Id_usu)
ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO telefonos (Id_telf, Telefono, Id_usu)
 VALUES ('101', '967334546', '001'), 
('102', '967348632', '002'),
('103', '914562532', '003');


CREATE TABLE clientes(
Dni VARCHAR(10) NOT NULL UNIQUE,
Descuento_venta FLOAT,
Id_usu CHAR(9),
CONSTRAINT CLI_IDU_PK PRIMARY KEY(Id_usu),
CONSTRAINT CLI_IDU_FK FOREIGN KEY(Id_usu) REFERENCES usuarios (Id_usu)
ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO clientes (Dni, Descuento_venta, Id_usu)
 VALUES ('48065241C', 0, '001'),
('20154478C', 0, '002'),
('68941252B', 0, '003'),
('12345678D', 0, '004');

CREATE TABLE articulos(
Codigo_art CHAR(9),
Nombre_art VARCHAR(50) NOT NULL UNIQUE,
Precio_actual FLOAT,
Descuento_actual FLOAT,
CONSTRAINT ART_COA_PK PRIMARY KEY(Codigo_art)
);


INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual)
VALUES ('1001', 'Muñeca', 20.5, 5),
('1002', 'Coche de carreras', 15.3, 2),
('1003', 'Tangram', 30.5, 1),
('1004', 'Lampara mesa', 60.65, 0),
('1005', 'Cuadro con flores', 80.5, 0),
('1006', 'Centro de mesa', 80.5, 0);

CREATE TABLE ventas(
Id_venta CHAR(9),
Fecha_ven DATE NOT NULL,
Moneda_venta VARCHAR(15) NOT NULL,
Id_usu CHAR(9) NOT NULL,
CONSTRAINT VEN_IDV_PK PRIMARY KEY(Id_venta),
CONSTRAINT VEN_IDU_FK FOREIGN KEY(Id_usu) REFERENCES clientes (Id_usu)
ON DELETE NO ACTION ON UPDATE NO ACTION
);


INSERT INTO ventas (Id_venta, Fecha_ven, Moneda_venta, Id_usu)
VALUES ('2002', '2019-12-15', 'EUROS', '003'),
('2001', '2019-12-14', 'EUROS', '001'),
('2003', '2019-12-13', 'DOLARES', '002');

CREATE TABLE lineas_venta(
N_linea_venta CHAR(9),
Cantidad_venta CHAR(10) NOT NULL,
Precio_venta FLOAT NOT NULL,
Desc_venta FLOAT,
Id_venta CHAR(9),
Codigo_art CHAR(9) NOT NULL,
CONSTRAINT LIV_NLV_PK PRIMARY KEY(N_linea_venta, Id_venta),
CONSTRAINT LIV_IDV_FK FOREIGN KEY(Id_venta) REFERENCES ventas (Id_venta)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT LIV_COA_FK FOREIGN KEY(Codigo_art) REFERENCES articulos (Codigo_art)
ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO lineas_venta (N_linea_venta, Cantidad_venta, Precio_venta, Desc_venta, Id_venta, Codigo_art)
VALUES ('001', '2', 20.5, 0, '2001', '1001'),
       ('002', '2', 15.3, 0, '2001', '1002'),
       ('003', '1', 30.5, 0, '2001', '1003');

CREATE TABLE proveedores(
Cif VARCHAR(10) NOT NULL UNIQUE,
Id_usu CHAR(9),
CONSTRAINT PRO_IDU_PK PRIMARY KEY(Id_usu),
CONSTRAINT PRO_IDU_FK FOREIGN KEY(Id_usu) REFERENCES usuarios(Id_usu)
ON DELETE CASCADE ON UPDATE CASCADE
);



INSERT INTO proveedores (Cif, Id_usu)
VALUES ('B01234567C', '001'),
('B23541253N', '002'),
('B89546325S', '003');

CREATE TABLE compras(
Id_compra CHAR(9),
Fecha_compra DATE NOT NULL,
Id_usu CHAR(9) NOT NULL,
Moneda_compra VARCHAR(15) NOT NULL,
CONSTRAINT COM_IDC_PK PRIMARY KEY(Id_compra),
CONSTRAINT COM_IDU_FK FOREIGN KEY(Id_usu) REFERENCES proveedores(Id_usu)
ON DELETE NO ACTION ON UPDATE NO ACTION
);


INSERT INTO compras (Id_compra, Fecha_compra, Moneda_compra, Id_usu)
VALUES ('3001', '2019-12-10', 'EUROS', '001'),
('3002', '2019-12-11', 'EUROS', '002'),
('3003', '2019-12-12', 'DOLARES', '003');

CREATE TABLE lineas_compra(
N_linea_compra CHAR(9),
Cantidad_compra CHAR(10) NOT NULL,
Precio_compra FLOAT NOT NULL,
Id_compra CHAR(9),
Codigo_art CHAR(9) NOT NULL,
CONSTRAINT LIC_NLC_PK PRIMARY KEY(N_linea_compra, Id_compra),
CONSTRAINT LIC_IDC_FK FOREIGN KEY(Id_compra) REFERENCES compras (Id_compra)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT LIC_COA_FK FOREIGN KEY(Codigo_art) REFERENCES articulos (Codigo_art)
ON DELETE CASCADE ON UPDATE CASCADE
);



INSERT INTO lineas_compra (N_linea_compra, Cantidad_compra, Precio_compra, 
Id_compra, Codigo_art)
VALUES ('001', '2', '10.5', '3001', '1001'),
('002', '4', '10.2', '3001', '1002'),
('003', '1', '10.5', '3001', '1003');


CREATE TABLE tipos(
Id_tipo CHAR(9),
Nombre_tipo VARCHAR(50) NOT NULL,
CONSTRAINT TIP_IDT_PK PRIMARY KEY(Id_tipo)
);



INSERT INTO tipos (Id_tipo, Nombre_tipo)
VALUES ('4001', 'preescolar'),
('4002', 'movimiento'),
('4003', 'educativos'),
('4004', 'decoración');

CREATE TABLE tipos_articulos(
Id_tipo CHAR(9),
Codigo_art CHAR(9),
CONSTRAINT TIA_IDC_PK PRIMARY KEY (Id_tipo, Codigo_art),
CONSTRAINT TIA_IDT_FK FOREIGN KEY (Id_tipo) REFERENCES tipos(Id_tipo)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT TIA_COA_FK FOREIGN KEY (Codigo_art) REFERENCES articulos(Codigo_art)
ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO tipos_articulos (Id_tipo, Codigo_art)
VALUES ('4001', '1001'),
('4002', '1002'),
('4003', '1003'),
('4004', '1004'),
('4004', '1005'),
('4004', '1006');


CREATE TABLE articulo_recomendado(
Codigo_art CHAR(9),
Codigo_art_recomendado CHAR(9),
CONSTRAINT ARR_CAR_PK PRIMARY KEY (Codigo_art, Codigo_art_recomendado),
CONSTRAINT ARR_COA_FK FOREIGN KEY (Codigo_art) REFERENCES articulos(Codigo_art),
CONSTRAINT ARR_COAF_FK FOREIGN KEY (Codigo_art_recomendado) REFERENCES articulos(Codigo_art)
ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO articulo_recomendado (Codigo_art, Codigo_art_recomendado)
 VALUES ('1004', '1005'),
 ('1004', '1006'),
 ('1001', '1002');
