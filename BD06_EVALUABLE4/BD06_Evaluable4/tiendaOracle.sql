--ORACLE
--CREACI�N BBDD EN RUN SQL
CREATE TABLESPACE eva4_ud6 DATAFILE 
'C:\oraclexe\app\oracle\oradata\XE\eva4_ud6.dbf' SIZE
20M AUTOEXTEND ON NEXT 50M MAXSIZE 300M 
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 10M 
SEGMENT SPACE MANAGEMENT AUTO;

--CREACION Usuario
CREATE USER eva4_und6 IDENTIFIED BY eva4_ud6 DEFAULT TABLESPACE eva4_ud6;

--CREACION privilegios
GRANT CONNECT, RESOURCE TO eva4_und6;

--BORRAR TABLAS
DROP TABLE tipos_articulos;
DROP TABLE tipos;
DROP TABLE articulo_recomendado;
DROP TABLE telefonos;
DROP TABLE lineas_venta;
DROP TABLE ventas;
DROP TABLE lineas_compra;
DROP TABLE articulos;
DROP TABLE compras;
DROP TABLE proveedores;
DROP TABLE clientes_ev4;
DROP TABLE usuarios;

-- CREACION TABLA USUARIOS
CREATE TABLE usuarios(
Id_usu CHAR(9),
Nombre VARCHAR(50),
Primer_apellido VARCHAR(50),
Segundo_apellido VARCHAR(50),
Ciudad VARCHAR(50),
CP CHAR(6),
Calle VARCHAR(50),
Numero VARCHAR(10),
Piso VARCHAR(10),
Pta VARCHAR(10),
Usuario VARCHAR(15) NOT NULL UNIQUE,
Password VARCHAR(15) NOT NULL,
Email VARCHAR(50),
CONSTRAINT USU_IDU_PK PRIMARY KEY(Id_usu)
);

INSERT INTO usuarios (Id_usu, Nombre, Primer_apellido, Segundo_apellido, Ciudad, CP, Calle, Numero, Piso, Pta, Usuario, Password, Email) VALUES ('001', 'Jos�', 'Alfaro', 'Garc�a', 'Albacete', '02006','Mar�a Mar�n', '57', '03', 'B', 'Pepin33', 'pepin', 'pepin@hotmail.es');
INSERT INTO usuarios (Id_usu, Nombre, Primer_apellido, Segundo_apellido, Ciudad, CP, Calle, Numero, Piso, Pta, Usuario, Password, Email) VALUES('002', 'Miguel', 'Perez', 'Pena', 'Albacete', '02007','Perez Pastor', '41', '02', 'A', 'Mike47', '1234', 'mikel@hotmail.es');
INSERT INTO usuarios (Id_usu, Nombre, Primer_apellido, Segundo_apellido, Ciudad, CP, Calle, Numero, Piso, Pta, Usuario, Password, Email) VALUES('003', 'Ana', 'Ponce', 'Garc�a', 'Madrid', '28001','Gran Via', '07', '10', 'F', 'Anika12', '0123456', 'anika@hotmail.es');
INSERT INTO usuarios (Id_usu, Nombre, Primer_apellido, Segundo_apellido, Ciudad, CP, Calle, Numero, Piso, Pta, Usuario, Password, Email) VALUES('004', 'Antonio', 'Fernandez', 'Ruiz', 'Valencia', '46008','San Vicente', '11', '5', 'F', 'antru', '7777777', 'antru@gmail.es');

-- CREACION TABLA telefonos
CREATE TABLE telefonos(
Id_telf CHAR(9),
Telefono VARCHAR(20),
Id_usu CHAR(9),
CONSTRAINT TEL_IDT_PK PRIMARY KEY(Id_telf),
CONSTRAINT TEL_IDU_FK FOREIGN KEY(Id_usu) REFERENCES usuarios (Id_usu)
ON DELETE CASCADE
);

INSERT INTO telefonos (Id_telf, Telefono, Id_usu) VALUES ('101', '967334546', '001');
INSERT INTO telefonos (Id_telf, Telefono, Id_usu) VALUES ('102', '967348632', '002');
INSERT INTO telefonos (Id_telf, Telefono, Id_usu) VALUES ('103', '914562532', '003');

--CREACION TABLA clientes
CREATE TABLE clientes (
Dni VARCHAR(10) NOT NULL UNIQUE,
Descuento_venta FLOAT,
Id_usu CHAR(9),
CONSTRAINT CLI_IDU_PK PRIMARY KEY(Id_usu),
CONSTRAINT CLI_IDU_FK FOREIGN KEY(Id_usu) REFERENCES usuarios (Id_usu)
ON DELETE CASCADE
);

INSERT INTO clientes (Dni, Descuento_venta, Id_usu) VALUES ('48065241C', 0, '001');
INSERT INTO clientes (Dni, Descuento_venta, Id_usu) VALUES ('20154478C', 0, '002');
INSERT INTO clientes (Dni, Descuento_venta, Id_usu) VALUES ('68941252B', 0, '003');
INSERT INTO clientes (Dni, Descuento_venta, Id_usu) VALUES ('123456789', 0, '004');

-- CREACION TABLA articulos
CREATE TABLE articulos(
Codigo_art CHAR(9),
Nombre_art VARCHAR(50) NOT NULL UNIQUE,
Precio_actual FLOAT,
Descuento_actual FLOAT,
CONSTRAINT ART_COA_PK PRIMARY KEY(Codigo_art)
);

INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual) VALUES ('1001', 'Mu�eca', 20.5, 5);
INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual) VALUES ('1002', 'Coche de carreras', 15.3, 2);
INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual) VALUES ('1003', 'Tangram', 30.5, 1);
INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual) VALUES ('1004', 'Lampara mesa', 60.65, 0);
INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual) VALUES ('1005', 'Cuadro con flores', 80.5, 0);
INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual) VALUES ('1006', 'Centro de mesa', 80.5, 0);

-- CREACION TABLA Ventas
CREATE TABLE ventas(
Id_venta CHAR(9),
Fecha_ven DATE NOT NULL,
Moneda_venta VARCHAR(15) NOT NULL,
Id_usu CHAR(9) NOT NULL,
CONSTRAINT VEN_IDV_PK PRIMARY KEY(Id_venta),
CONSTRAINT VEN_IDU_FK FOREIGN KEY(Id_usu) REFERENCES clientes (Id_usu)
ON DELETE CASCADE
);

INSERT INTO ventas (Id_venta, Fecha_ven, Moneda_venta, Id_usu) VALUES ('2002', '15/12/2019', 'EUROS', '003');
INSERT INTO ventas (Id_venta, Fecha_ven, Moneda_venta, Id_usu) VALUES ('2001', '14/12/2019', 'EUROS', '001');
INSERT INTO ventas (Id_venta, Fecha_ven, Moneda_venta, Id_usu) VALUES ('2003', '13/12/2019', 'DOLARES', '002');

-- CREACION TABLA lineas_venta
CREATE TABLE lineas_venta(
N_linea_venta CHAR(9),
Cantidad_venta CHAR(10) NOT NULL,
Precio_venta FLOAT NOT NULL,
Desc_venta FLOAT,
Id_venta CHAR(9),
Codigo_art CHAR(9) NOT NULL,
CONSTRAINT LIV_NLV_PK PRIMARY KEY(N_linea_venta, Id_venta),
CONSTRAINT LIV_IDV_FK FOREIGN KEY(Id_venta) REFERENCES ventas (Id_venta)
ON DELETE CASCADE,
CONSTRAINT LIV_COA_FK FOREIGN KEY(Codigo_art) REFERENCES articulos (Codigo_art)
ON DELETE CASCADE
);

INSERT INTO lineas_venta (N_linea_venta, Cantidad_venta, Precio_venta, Desc_venta, Id_venta, Codigo_art) VALUES ('001', '2', 20.5, 0, '2001', '1001');
INSERT INTO lineas_venta (N_linea_venta, Cantidad_venta, Precio_venta, Desc_venta, Id_venta, Codigo_art) VALUES ('002', '2', 15.3, 0, '2001', '1002');
INSERT INTO lineas_venta (N_linea_venta, Cantidad_venta, Precio_venta, Desc_venta, Id_venta, Codigo_art) VALUES ('003', '1', 30.5, 0, '2001', '1003');

-- CREACION TABLA proveedores
CREATE TABLE proveedores(
Cif VARCHAR(10) NOT NULL UNIQUE,
Id_usu CHAR(9),
CONSTRAINT PRO_IDU_PK PRIMARY KEY(Id_usu),
CONSTRAINT PRO_IDU_FK FOREIGN KEY(Id_usu) REFERENCES usuarios(Id_usu)
ON DELETE CASCADE
);

INSERT INTO proveedores (Cif, Id_usu) VALUES ('B01234567C', '001');
INSERT INTO proveedores (Cif, Id_usu) VALUES ('B23541253N', '002');
INSERT INTO proveedores (Cif, Id_usu) VALUES ('B89546325S', '003');

-- CREACION TABLA compras
CREATE TABLE compras(
Id_compra CHAR(9),
Fecha_compra DATE NOT NULL,
Id_usu CHAR(9) NOT NULL,
Moneda_compra VARCHAR(15) NOT NULL,
CONSTRAINT COM_IDC_PK PRIMARY KEY(Id_compra),
CONSTRAINT COM_IDU_FK FOREIGN KEY(Id_usu) REFERENCES proveedores(Id_usu)
ON DELETE CASCADE
);

INSERT INTO compras (Id_compra, Fecha_compra, Moneda_compra, Id_usu) VALUES ('3001', '10/12/2019', 'EUROS', '001');
INSERT INTO compras (Id_compra, Fecha_compra, Moneda_compra, Id_usu) VALUES ('3002', '11/12/2019', 'EUROS', '002');
INSERT INTO compras (Id_compra, Fecha_compra, Moneda_compra, Id_usu) VALUES ('3003', '12/12/2019', 'DOLARES', '003');

-- CREACION TABLA lineas_compra
CREATE TABLE lineas_compra(
N_linea_compra CHAR(9),
Cantidad_compra CHAR(10) NOT NULL,
Precio_compra FLOAT NOT NULL,
Id_compra CHAR(9),
Codigo_art CHAR(9) NOT NULL,
CONSTRAINT LIC_NLC_PK PRIMARY KEY(N_linea_compra, Id_compra),
CONSTRAINT LIC_IDC_FK FOREIGN KEY(Id_compra) REFERENCES compras (Id_compra)
ON DELETE CASCADE,
CONSTRAINT LIC_COA_FK FOREIGN KEY(Codigo_art) REFERENCES articulos (Codigo_art)
ON DELETE CASCADE
);

INSERT INTO lineas_compra (N_linea_compra, Cantidad_compra, Precio_compra, Id_compra, Codigo_art) VALUES ('001', '2', 10.5, '3001', '1001');
INSERT INTO lineas_compra (N_linea_compra, Cantidad_compra, Precio_compra, Id_compra, Codigo_art) VALUES ('002', '4', 10.2, '3001', '1002');
INSERT INTO lineas_compra (N_linea_compra, Cantidad_compra, Precio_compra, Id_compra, Codigo_art) VALUES ('003', '1', 10.5, '3002', '1003');

--CREACION TABLA tipos
CREATE TABLE tipos(
Id_tipo CHAR(9),
Nombre_tipo VARCHAR(50) NOT NULL,
CONSTRAINT TIP_IDT_PK PRIMARY KEY(Id_tipo)
);

INSERT INTO tipos (Id_tipo, Nombre_tipo) VALUES ('4001', 'preescolar');
INSERT INTO tipos (Id_tipo, Nombre_tipo) VALUES ('4002', 'movimiento');
INSERT INTO tipos (Id_tipo, Nombre_tipo) VALUES ('4003', 'educativos');
INSERT INTO tipos (Id_tipo, Nombre_tipo) VALUES ('4004', 'decoraci�n');

-- CREACION TABLA tipos_articulos
CREATE TABLE tipos_articulos(
Id_tipo CHAR(9),
Codigo_art CHAR(9),
CONSTRAINT TIA_IDC_PK PRIMARY KEY (Id_tipo, Codigo_art),
CONSTRAINT TIA_IDT_FK FOREIGN KEY (Id_tipo) REFERENCES tipos(Id_tipo)
ON DELETE CASCADE,
CONSTRAINT TIA_COA_FK FOREIGN KEY (Codigo_art) REFERENCES articulos(Codigo_art)
ON DELETE CASCADE 
);

INSERT INTO tipos_articulos (Id_tipo, Codigo_art) VALUES ('4001', '1001');
INSERT INTO tipos_articulos (Id_tipo, Codigo_art) VALUES ('4002', '1002');
INSERT INTO tipos_articulos (Id_tipo, Codigo_art) VALUES ('4003', '1003');
INSERT INTO tipos_articulos (Id_tipo, Codigo_art) VALUES ('4004', '1004');
INSERT INTO tipos_articulos (Id_tipo, Codigo_art) VALUES ('4004', '1005');
INSERT INTO tipos_articulos (Id_tipo, Codigo_art) VALUES ('4004', '1006');

-- CREACION TABLA articulo_recomendado
CREATE TABLE articulo_recomendado(
Codigo_art CHAR(9),
Codigo_art_recomendado CHAR(9),
CONSTRAINT ARR_CAR_PK PRIMARY KEY (Codigo_art, Codigo_art_recomendado),
CONSTRAINT ARR_COA_FK FOREIGN KEY (Codigo_art) REFERENCES articulos(Codigo_art),
CONSTRAINT ARR_COAF_FK FOREIGN KEY (Codigo_art_recomendado) REFERENCES articulos(Codigo_art)
ON DELETE CASCADE
);

INSERT INTO articulo_recomendado (Codigo_art, Codigo_art_recomendado) VALUES ('1004', '1005');
INSERT INTO articulo_recomendado (Codigo_art, Codigo_art_recomendado) VALUES ('1004', '1006');
INSERT INTO articulo_recomendado (Codigo_art, Codigo_art_recomendado) VALUES ('1001', '1002');
