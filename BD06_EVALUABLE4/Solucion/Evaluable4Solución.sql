


-- Consulta IMPORTE DE CADA COMPRA. FUNCION AGREGADA
-- Mostrar el id de la compra, el id del usuario, el nombre del usuario y el importe totoal de la compra.

select com.id_compra, com.id_usu, usu.nombre , sum(lin.cantidad_compra * lin.precio_compra ) as importe_total
from compras as com, lineas_compra as lin, usuarios as usu
where com.id_compra = lin.id_compra and com.id_usu=usu.id_usu
group by com.id_compra
order by com.id_compra, lin.n_linea_compra

-- ConsultA. IMPORTE ARTICULO. GROUP BY
-- Mostrar del articulo el codigo, nombre y el importe total comprado en todas las compras.

SELECT art.Codigo_art, art.Nombre_art, sum( lin.Cantidad_compra* lin.Precio_compra) as importe_comprado
FROM articulos as art, lineas_compra as lin
WHERE art.Codigo_art=lin.Codigo_art
GROUP BY art.Codigo_art, art.Nombre_art
ORDER BY art.Codigo_art



-- Consulta  PVP. UPDATE
-- Hoy en 9 de octubre y hay que hacer descuento del 8% a los articulos que sean de decoración.
update  articulos, tipos, tipos_articulos
set articulos.descuento_actual=8
where  articulos.Codigo_art=tipos_articulos.Codigo_art 
and  tipos_articulos.Id_tipo=tipos.Id_tipo
and tipos.Nombre_tipo='Decoración';



-- Consulta  PVP. 
-- Calcular el pvp ventas del articulo articulo cuya fecha sea 14/12/2019.

select  l.Codigo_art , v.fecha_ven, nombre_art, precio_actual, descuento_actual, precio_actual *  ( 1 - descuento_actual/100)
from articulos  a ,lineas_venta  l, ventas  v
where a.Codigo_art = l.Codigo_art and v.Id_venta=l.Id_venta AND fecha_ven='2019-12-14'; 


-- Update
-- Si el cliente es mi hermano con dni 123456789 entonces, se le hace un descuento del 25% en el importa de su compra.  Nos olvidamos
-- del descuento por defecto del articulo.

update clientes, usuarios
 set clientes.Descuento_venta = 25
 where usuarios.Id_usu = clientes.Id_usu and clientes.Dni='123456789';

-- Calcular el beneficio del año 2019
create or replace view importe_ventas AS
 select sum(vl.cantidad_venta * vl.Precio_venta ) as importe_ventas
 from ventas as v, lineas_venta as vl
 where v.Id_venta = vl.Id_venta;

create  or replace view importe_compras AS
 select sum(cl.Cantidad_compra * cl.Precio_compra ) as importe_compras 
 from compras as c, lineas_compra as cl
 where c.id_compra = cl.id_compra;

select importe_ventas, importe_compras, (importe_ventas - importe_compras) as beneficio
from importe_ventas , importe_compras;













