INSERT INTO usuarios (Id_usu, Nombre, Primer_apellido, Segundo_apellido, 
Ciudad, CP, Calle, Numero, Piso, Pta, Usuario, Password, Email)
VALUES ('001', 'José', 'Alfaro', 'García', 'Albacete', '02006',
'María Marín', '57', '03', 'B', 'Pepin33', 'pepin', 'pepin@hotmail.es'),
('002', 'Miguel', 'Perez', 'Pena', 'Albacete', '02007',
'Perez Pastor', '41', '02', 'A', 'Mike47', '1234', 'mikel@hotmail.es'),
('003', 'Ana', 'Ponce', 'García', 'Madrid', '28001',
'Gran Via', '07', '10', 'F', 'Anika12', '0123456', 'anika@hotmail.es');

INSERT INTO telefonos (Id_telf, Telefono, Id_usu)
VALUES ('101', '967334546', '001'), 
('102', '967348632', '002'),
('103', '914562532', '003');

INSERT INTO clientes (Dni, Descuento_venta, Id_usu)
VALUES ('48065241C', NULL, '001'),
('20154478C', NULL, '002'),
('68941252B', NULL, '003');

INSERT INTO articulos (Codigo_art, Nombre_art, Precio_actual, Descuento_actual)
VALUES ('1001', 'Muñeca', 20.5, 5),
('1002', 'Coche de carreras', 15.3, 2),
('1003', 'Tangram', 30.5, 1);

INSERT INTO ventas (Id_venta, Fecha_ven, Moneda_venta, Id_usu)
VALUES ('2002', '2019-12-15', 'EUROS', '003'),
('2001', '2019-12-14', 'EUROS', '001'),
('2003', '2019-12-13', 'DOLARES', '002');

INSERT INTO lineas_venta (N_linea_venta, Cantidad_venta, Precio_venta, Desc_venta, 
Id_venta, Codigo_art)
VALUES ('001', '2', 20.5, NULL, '2001', '1001'),
('002', '2', 15.3, 10, '2001', '1002'),
('003', '1', 30.5, NULL, '2001', '1003');

INSERT INTO proveedores (Cif, Id_usu)
VALUES ('B01234567C', '001'),
('B23541253N', '002'),
('B89546325S', '003');

INSERT INTO compras (Id_compra, Fecha_compra, Moneda_compra, Id_usu)
VALUES ('3001', '2019-12-10', 'EUROS', '001'),
('3002', '2019-12-11', 'EUROS', '002'),
('3003', '2019-12-12', 'DOLARES', '003');

INSERT INTO lineas_compra (N_linea_compra, Cantidad_compra, Precio_compra, 
Id_compra, Codigo_art)
VALUES ('001', '2', '10.5', '3001', '1001'),
('002', '4', '10.2', '3001', '1002'),
('003', '1', '10.5', '3001', '1003');

INSERT INTO tipos (Id_tipo, Nombre_tipo)
VALUES ('4001', 'preescolar'),
('4002', 'movimiento'),
('4003', 'educativos');

INSERT INTO tipos_articulos (Id_tipo, Codigo_art)
VALUES ('4001', '1001'),
('4002', '1002'),
('4003', '1003');

INSERT INTO articulo_recomendado (Codigo_art, Codigo_art_recomendado)
VALUES ('1001', '1002'),
('1002', '1001'),
('1003', '1002');
