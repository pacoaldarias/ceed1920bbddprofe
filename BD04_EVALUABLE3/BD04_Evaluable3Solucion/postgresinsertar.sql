INSERT INTO usuarios (id_usu, nombre , primer_apellido, segundo_apellido, ciudad, cp, calle, numero, piso, pta, usuario, password, email)
VALUES
 ('00001','cristina','chiarri','toumit','valencia',46004,'grabador esteve','14', '7', '4','Crischitu','Crischitu14','cristinachiarri@gmail.com'),
 ('00002','maria','gomez','perez','cordoba','13000','paraiso','15','4','8','Marivi','Matu75','marivi@hotmail.com'),
 ('00003','felipe','villanueva','sanchez','lliria','23150','correos','10','1','2','Fevisa','dahlia81','fevisa@yahoo.es'),
 ('00004','ana','gil','fernandez','valencia',46003,'colon','58', '7', '4','AnaGil95','Anita95','agf95@gmail.com'),
 ('00005','mario','gomez','ferrer','alicante','55555','serpis','35','4','8','MarioGF','MgomezF','mariogofer@hotmail.com'),
 ('00006','angela','romero','torres','Torrent','43000','valencia','21','2','3','angelarom','Emma13','anrometo@yahoo.es');

INSERT INTO telefonos (id_telf, telefono, id_usu)
VALUES
('00001','963523890','00001'),
('00003','963531080','00003'),
('00002','962741522','00001');

INSERT INTO clientes (dni, descuento_venta, id_usu)
VALUES
('14382278E',10,'00002'),
('25498831T',15,'00003');

INSERT INTO clientes (dni, id_usu)
VALUES
('22946675R','00001');

INSERT INTO ventas (id_ven, fecha_venta, moneda_venta, id_usu)
VALUES
('00001','2019/10/13','EUR','00001'),
('00002','2019/11/14','EUR','00001'),
('00003','2019/11/20','EUR','00002');

INSERT INTO proveedores (cif, id_usu)
VALUES
('A13264072','00001'),
('E88645601','00002'),
('E09187188','00003');

INSERT INTO compras (id_compra, fecha_compra, moneda_compra, id_usu)
VALUES
('00001','2019/12/02','EUR','00001'),
('00002','2019/12/10','EUR','00002'),
('00003','2019/12/02','EUR','00003'),
('00004','2019/12/03','EUR','00001'),
('00005','2019/12/10','EUR','00002'),
('00006','2019/10/02','EUR','00002');

INSERT INTO tipos (id_tipo, nombre_tipo)
VALUES
('00004','cosmetica'),
('00078','hogar'),
('00017','accesorios'),
('00001','moda'),
('00021','jardin');

INSERT INTO articulos (codigo_art,nombre_art,precio_actual,descuento_actual,id_tipo)
VALUES
('89510','protector solar',8.25,10,'00004'),
('12564','mesa hawai',75,20,'00078'),
('21487','toldo vela',36.50,0,'00021'),
('59742','paraguas',10,10,'00017'),
('54220','pijama marvel',25.50,12,'00001'),
('03651','calcetines niño',6.35,5,'00001');

INSERT INTO lineas_venta (lin_ven,cantidad_ven,precio_ven,desc_ven,id_ven,codigo_art)
VALUES
('00001',1,8.25,0,'00001','89510'),
('00003',1,75,5,'00001','12564'),
('00001',1,36.50,0,'00002','21487');

INSERT INTO lineas_compra (lin_com,cantidad_com,precio_com,id_compra,codigo_art)
VALUES
(00001,20,5,'00004','59742'),
(00001,20,12,'00005','54220'),
(00002,50,2,'00004','03651');

INSERT INTO articulorecomendado (codigo_art,codigo_artrecom)
VALUES
('12564','21487'),
('54220','03651');





