CREATE TABLE usuarios(
id_usu char(5),
nombre varchar(50) DEFAULT NULL, 
primer_apellido varchar(50) DEFAULT NULL, 
segundo_apellido varchar(50) DEFAULT NULL,  
ciudad varchar(50) DEFAULT NULL, 
cp int DEFAULT NULL,
calle varchar(50) DEFAULT NULL,
numero varchar(10) DEFAULT NULL,
piso varchar(10) DEFAULT NULL,
pta varchar(10) DEFAULT NULL,
usuario varchar(15) NOT NULL  UNIQUE,
password varchar(15) NOT NULL,
email varchar(50) DEFAULT NULL,
CONSTRAINT usu_idusu_pk PRIMARY KEY (id_usu)
);

CREATE TABLE telefonos(
id_telf char(5),
telefono varchar(10) DEFAULT NULL UNIQUE,
id_usu char(5) NOT NULL,
CONSTRAINT tel_idtel_pk PRIMARY KEY (id_telf),
CONSTRAINT tel_idusu_fk FOREIGN KEY (id_usu) REFERENCES usuarios (id_usu)
);

CREATE TABLE clientes (
id_usu char(5),
dni char(10) NOT NULL UNIQUE,
descuento_venta integer DEFAULT NULL,
CONSTRAINT cli_idusu_pk PRIMARY KEY (id_usu),
CONSTRAINT cli_idusu_fk FOREIGN KEY (id_usu) REFERENCES usuarios (id_usu)
);

CREATE TABLE ventas(
id_ven char(5),     
fecha_venta date NOT NULL,
moneda_venta varchar(15) NOT NULL,
id_usu char(5) NOT NULL,
CONSTRAINT ven_idven_pk PRIMARY KEY (id_ven),
CONSTRAINT ven_idusu_fk FOREIGN KEY (id_usu) REFERENCES clientes (id_usu)
);

CREATE TABLE proveedores (
id_usu char(5),
cif char(10) NOT NULL UNIQUE,
CONSTRAINT pro_idusu_pk PRIMARY KEY (id_usu),
CONSTRAINT pro_idusu_fk FOREIGN KEY (id_usu) REFERENCES usuarios (id_usu)
);

CREATE TABLE compras (
id_compra char(5),  
fecha_compra date NOT NULL,
moneda_compra varchar(15) NOT NULL,
id_usu char(5) NOT NULL,
CONSTRAINT com_idcom_pk PRIMARY KEY (id_compra),
CONSTRAINT com_idusu_fk FOREIGN KEY (id_usu) REFERENCES proveedores (id_usu)
);

CREATE TABLE tipos (
id_tipo char(5),
nombre_tipo varchar (30) NOT NULL,
CONSTRAINT tip_idtip_pk PRIMARY KEY (id_tipo)
);

CREATE TABLE articulos (
codigo_art char(5),  
nombre_art varchar (50) NOT NULL,  
precio_actual numeric(5,2) NOT NULL, 
descuento_actual integer DEFAULT NULL,
id_tipo char(5),
CONSTRAINT art_codart_pk PRIMARY KEY (codigo_art),
CONSTRAINT art_idtip_fk FOREIGN KEY (id_tipo) REFERENCES tipos (id_tipo) ON DELETE SET NULL  ON UPDATE CASCADE
);

CREATE TABLE lineas_venta (
id_ven char(5),
lin_ven char(5), 
cantidad_ven int NOT NULL, 
precio_ven numeric(5,2) NOT NULL, 
desc_ven int DEFAULT NULL,
codigo_art char(5) NOT NULL, 
CONSTRAINT lven_linven_idven_pk PRIMARY KEY (lin_ven,id_ven),
CONSTRAINT lven_idven_fk FOREIGN KEY (id_ven) REFERENCES ventas (id_ven) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT ven_codart_fk FOREIGN KEY (codigo_art) REFERENCES articulos (codigo_art)
);

CREATE TABLE lineas_compra (
id_compra char(5),
lin_com char(5),
cantidad_com integer NOT NULL,
precio_com numeric(4,2),
codigo_art char(5) NOT NULL,  
CONSTRAINT lcom_idcom_lin_com_pk PRIMARY KEY (id_compra,lin_com),
CONSTRAINT lcom_idcom_fk FOREIGN KEY (id_compra) REFERENCES compras (id_compra) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT lcom_codart_fk FOREIGN KEY (codigo_art) REFERENCES articulos (codigo_art) 
);

CREATE TABLE articulorecomendado (
codigo_art char(5),
codigo_artrecom char(5),
CONSTRAINT artrec_codart_codartrec_pk PRIMARY KEY (codigo_art,codigo_artrecom),
CONSTRAINT artrec_codart_fk FOREIGN KEY (codigo_art) REFERENCES articulos (codigo_art) ON DELETE CASCADE ON UPDATE CASCADE, 
CONSTRAINT artrec_codartrec_fk FOREIGN KEY (codigo_artrecom) REFERENCES articulos (codigo_art) ON DELETE CASCADE ON UPDATE CASCADE 
);











