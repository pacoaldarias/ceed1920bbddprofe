

-- pregunta 3
create dababase db1;
create role rol1;
create user usuario1 with password 'usuario1' default role rol1;
grant all privileges on db1.* to 'rol1'@'*' with grant options;

-- pregunta 4
create table persona ( 
idpersona numeric (3) primary key,
nombrepersona varchar(2),
idgrupo numeric (3),
idresponsable numeric(3),
constraint pk_persona primary key (idpersona),
constraint fk_pergru (idgrupo)  references grupo(idgrupo) on update cascade
constraint fk_respon  (idresponsable)  references persona(idpersona)
);


create table grupo (
idgrupo numeric (3) primary key,
nombregrupo varchar(20)
);


-- pregunta 5
insert into persona (idpersona, nombrepersona,idgrupo, idresponsable) values (1,'paco',1, ); 
insert into persona (idpersona, nombrepersona,idgrupo, idresponsable) values (2,'juan',1,1 );;
insert into persona (idpersona, nombrepersona,idgrupo, idresponsable) values (3,'pedro',2,1 );;


insert into grupo (idgrupo, nombregrupo) values (1,'grupo1'); 
insert into grupo (idgrupo, nombregrupo) values (1,'grupo2'); 

-- pregunta 6
select p1.idresponsable, p2.nombre, count( p1.idresponsable) 
from persona p1, persona p2
where p1.idresponsable = p2.id
group by  p1.idresponsable, p2.nombre


-- pregunta 7

CREATE TABLE MODIFICADOS(
id numeric (3) primary key,
idpresona numeric(3) ,
idresponsableold numeric(3),
idresponsablenew  numeric(3),
fecha timestamp,
);

CREATE SEQUENCE incrementoid
INCREMENT BY 1
START WITH 1



CREATE OR REPLACE TRIGGER responsable 
BEFORE UPDATE
ON PERSONA
FOR EACH ROW
DECLARE
BEGIN
IF :NEW. idresponsable != :OLD. idresponsable THEN
    INSERT INTO MODIFICADOS VALUES (incrementoid.NextVal, :OLD.idpresona, :OLD.idresponsable , :NEW.IDRESPONSABLE, USER, TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24'));
END IF;
END;

--UPDATE PERSONA SET idresponsable = 1 WHERE idresponsable = 2;
 
$$ 
 

