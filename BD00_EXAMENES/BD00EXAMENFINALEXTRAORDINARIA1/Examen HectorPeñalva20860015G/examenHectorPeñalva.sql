-- PREGUNTA 3. Administración de usuarios
CREATE DATABASE bd1;

CREATE ROLE administrador;

CREATE USER usuario1
IDENTIFIED BY usuario1;

GRANT administrador
TO usuario1
WITH ADMIN OPTION;


-- PREGUNTA 4. Diseño Físico. DDL

CREATE TABLE grupo (
idGrupo INTEGER,
nombreGrupo VARCHAR (50),
CONSTRAINT gru_idg_pk PRIMARY KEY (idGrupo) );

CREATE TABLE persona (
idPersona INTEGER,
nombrePersona VARCHAR (50),
idGrupo INTEGER NOT NULL,
CONSTRAINT per_idp_pk PRIMARY KEY (idPersona),
CONSTRAINT per_idg_fk FOREIGN KEY (idGrupo) REFERENCES grupo (idGrupo) );



-- PREGUNTA 5. Diseño Físico. DML


ALTER TABLE persona ADD idResponsable INTEGER NOT NULL;

INSERT INTO grupo (idGrupo, nombreGrupo)
VALUES (1, 'Grupo1'), (2, 'Grupo2')

INSERT INTO persona (idPersona, nombrePersona, idGrupo, idResponsable)
VALUES (1, 'JUAN',1, NULL), (2, 'PEDRO',1, 1)




-- PREGUNTA 6. Diseño Físico. DQL

SELECT DISTINCT nombrePersona, count(*) idResponsable 
FROM persona
WHERE idPersona = idResponsable